let inst_cat_lab = {
    "instituicoes": [{
            "nome": "USP ICMC",
            "nome_ext": "Universidade de São Paulo - ICMC",
            "imagem": "./imagens/uspicmc.jpg",
            "categorias": [{
                    "nomeCat": "Física",
                    "laboratorios": [{
                        "nomeLab": "Materiais Magnéticos",
                            "imagemLab": "materiaisMagneticos.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos especializados está apta a realizar estudos estruturais disversos, processos de produção especializada de microSQUIDs e outros tipo de nanopartículas relacionadas ao meio físico. Visamos a integração entre os temas de Magnetismo Ambiental, Vidros e Nanomateriais.</p><p>Como alguns experimentos, realizamos o estudo estrutural e magnético de filmes finos de CoPt e FePt com anisotropia perpendicular, de estruturas plasmônicas envolvendo filmes finos de Au e materiais magnéticos, de micromagnetismo experimental em partículas magnéticas microscópicas com o uso de microscopia ótica de varredura em campo próximo, além de realizarmos a caracterização de nanopartículas de óxidos de ferro em ferrofluidos.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Magnetometro",
                                    "imagemEquip": "magnetometro.jpg"
                                },
                                {
                                    "nomeEquip": "Forno Arco Voltaico",
                                    "imagemEquip": "voltaic_arc_furnace_argon_atmosphere.jpg"
                                },
                                {
                                    "nomeEquip": "Caixa de Luva",
                                    "imagemEquip": "Caixa_de_luva.jpg"
                                }
                            ]
                        },
                        {
                            "nomeLab": "Estado Sólido e Baixas Temperaturas",
                            "imagemLab": "BaixaTemperaturaeEstadoSolido.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos especializados realiza o estudo experimental de sistemas eletrônicos fortemente correlacionados, incluindo propriedades magnéticas e de transporte elétrico de óxidos de metais de transição e materiais magnéticos geometricamente frustrados.</p><p>Realizamos pesquisa em Novos Materiais envolvendo principalmente campos magnéticos intensos e baixas temperaturas. Os materiais estudados são: -nanoestruturas semicondutoras para aplicação em spintrônica e computação quântica; - materiais com forte correlação eletrônica (supercondutores e magnéticos). Além disso trabalhamos na construção de uma antena para ondas gravitacionais, resfriada a temperaturas da ordem de 0.1K.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Susceptômetro",
                                    "imagemEquip": "3_ac_susceptometer.jpg"
                                },
                                {
                                    "nomeEquip": "Magnetômetro",
                                    "imagemEquip": "magnetometro.jpg"
                                },
                                {
                                    "nomeEquip": "Refrigerador de Diluição Plastica",
                                    "imagemEquip": "refrigerador_diluicao_plastica.jpg"
                                }
                            ]
                        },
                        {
                            "nomeLab": "Magneto-Óptica e Espectroscopia Não-Linear",
                            "imagemLab": "MagneticNonLinearSpectro.JPG",
                            "descLab": "<p>Investigamos as possibilidades de controle dos efeitos quânticos em diversos tipos de estruturas artificiais, nas quais há restrição do movimento eletrônico em baixa dimensão. Técnicas de espectroscopia óptica e elétrica em baixas temperaturas e altos campos magnéticos são utilizadas para investigar as propriedades de estruturas artificiais desse tipo. Também são realizadas as simulações da resposta óptica, elétrica e magnética desses sistemas.</p><p>São focados os experimentos que utilizam equipamentos na área da óptica, elétrica e magnética, focados no estudo de materiais,podem ser executados neste laboratório.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Imã de Tesla",
                                    "imagemEquip": "tesla_magnet.JPG"
                                },
                                {
                                    "nomeEquip": "Espectrômetros",
                                    "imagemEquip": "spectrometers.JPG"
                                },
                                {
                                    "nomeEquip": "Espectroscopia em femtosegundos",
                                    "imagemEquip": "femtosecond_pump_probe.JPG"
                                }
                            ]
                        }
                    ]
                },
                {
                    "nomeCat": "Química",
                    "laboratorios": [{
                            "nomeLab": "Cristalografia",
                            "imagemLab": "fap-cristalografia.jpg",
                            "descLab": "<p>O Laboratório de Cristalografia tem como objetivos a pesquisa em física da matéria condensada, ciência dos materiais e áreas interdisciplinares com ênfase em propriedades estruturais: estudos de monocristais, policristais, sólidos amorfos, cristais líquidos, polímeros, géis, sistemas micelares e proteínas em solução.</p><p> São realizadas técnicas experimentais principalmente nas áreas de difração e espalhamento de raios X, associadas com outros métodos auxiliares.</p><p> É dada ênfase em materiais usados em dispositivos ópticos e eletrônicos e em sistemas complexos com ordem supra-molecular e utilizadas também fontes convencionais de raios X e de radiação síncrotron.</p><p> É realizada também a prestação de serviços à comunidade na área de caracterização de materiais.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Analisador de Porosidade",
                                    "imagemEquip": "Analisador_de_Porosidade.jpg"
                                },
                                {
                                    "nomeEquip": "Difratômetro de Monocristal",
                                    "imagemEquip": "Difratômetro_de_Monocristal.jpg"
                                },
                                {
                                    "nomeEquip": "Nanostar",
                                    "imagemEquip": "Nanostar.jpg"
                                }
                            ]
                        },
                        {
                            "nomeLab": "Análise de Materiais por Feixes Iônicos",
                            "imagemLab": "fap-lamfi.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos está apta a colaborar com o desenvolvimento e aplicação de métodos analíticos nucleares para análise e caracterização de materiais e superfícies. São realizados estudos de efeitos da topografia de filmes finos e porosidade de materiais em espectros RBS.</p><p> A equipe trabalha com precisão, acurácia e dependência da base de dados (secção de choque de ionização, poder de freamento e coeficientes de absorção de massa) de análises PIXE de pós, preparados na forma de amostras espessas. Além disso também são estudados fenômenos de passivação de superfícies contra oxidação, manutenção e aperfeiçoamento das instalações do LAMFI para disponibilizar e divulgar modernos recursos analíticos à comunidade científica.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Acelerador e Fonte de Íons",
                                    "imagemEquip": "acelerador_e_fontes_de_ions.JPG"
                                }
                            ]
                        }
                    ]
                }

            ]
        },
        {
            "nome": "USP Butantã",
            "nome_ext": "Universidade de São Paulo - Butantã",
            "imagem": "./imagens/usp.jpg",
            "categorias": [{
                    "nomeCat": "Ciências Farmacêuticas",
                    "laboratorios": [{
                        "nomeLab": "Microbiologia de Alimentos",
                        "imagemLab": "nutribiologia.jpg",
                        "descLab": "<p>Nossa equipe é composta por pesquisadores e técnicos especializados para a prestação de serviços à comunidade, realizando análises laboratoriais de composição nutricional e controle de qualidade de alimentos. Dentre as análises realizadas destacamos ecologia microbiana dos alimentos,  avaliação de risco microbiológico, microbiologia preditiva, avaliação da qualidade e segurança microbiológica dos alimentos por métodos convencionais e biologia molecular - o controle do desenvolvimento microbiano através de métodos físicos e de bioconservação.</p><p> Estes procedimentos podem ser realizados em diversos tipos de alimento com propósito analítico para a caracterização de propriedades e composição.<p>",
                        "equipLab": [
                            {
                                "nomeEquip": "Centrífuga para Microtubos",
                                "imagemEquip": "centrifugademicrotubos.png"
                            }
                        ]
                    },
                    {
                        "nomeLab": "Lípides",
                        "imagemLab": "laboratoriolipides.jpg",
                        "descLab": "<p>OLaboratório de Lípides tem como foco principal a análise e caracterização de lípides, destacando-se os ácidos graxos saturados, polinsaturados, trans e conjugados. Também temos desenvolvido trabalhos verificando-se a ação biológica de compostos bioativos presentes em diferentes alimentos como óleos (óleos de romã, melão de são caetano, linhaça, semente de uva, pequi), ácidos graxos poli-insaturados isolados (ácido linoléico conjugado), frutos (romã, caju, pequi), especiarias (orégano, alecrim), algas; além de resíduos do processamento de alimentos como semente de maracujá, sobre marcadores de inflamação, de estresse oxidativo e metabolismo lipídico.</p>",
                        "equipLab": [
                            {
                                "nomeEquip": "Extrator de lipídios",
                                "imagemEquip": "extrator-de-lipidios.png"
                            }
                        ]
                    },
                    {
                        "nomeLab": "Nutrigênomica",
                        "imagemLab": "nutrigenomica.jpg",
                        "descLab": "<p>O grupo do Nutrigenômica e Programação desenvolve pesquisas para avaliar a influência da nutrição na expressão gênica. Intervenções nutricionais em estudo incluem consumo de dietas hiperlipídicas, deficientes ou suplementadas com micronutrientes, bem como suplementadas com compostos polifenólicos de frutas. As janelas de intervenção nutricional incluem pré-concepção, gestação e lactação. São utilizados modelos in vitro e in vivo e analisados os mecanismos celulares e moleculares associados à programação nutricional do risco de câncer de mama.</p><p> O laboratório é útil para experimentos acerca da interação entre elementos genéticos e a correlação das consequências futuras com a existência ou não de influência nutricional.</p>",
                        "equipLab": [
                            {
                                "nomeEquip": "Termociclador",
                                "imagemEquip": "termociclador.jpg"
                            }
                        ]
                    }

                    ]
                },
                {
                    "nomeCat": "Química",
                    "laboratorios": [{
                        "nomeLab": "Cristalografia",
                            "imagemLab": "fap-cristalografia.jpg",
                            "descLab": "<p>O Laboratório de Cristalografia tem como objetivos a pesquisa em física da matéria condensada, ciência dos materiais e áreas interdisciplinares com ênfase em propriedades estruturais: estudos de monocristais, policristais, sólidos amorfos, cristais líquidos, polímeros, géis, sistemas micelares e proteínas em solução.</p><p> São realizadas técnicas experimentais principalmente nas áreas de difração e espalhamento de raios X, associadas com outros métodos auxiliares.</p><p> É dada ênfase em materiais usados em dispositivos ópticos e eletrônicos e em sistemas complexos com ordem supra-molecular e utilizadas também fontes convencionais de raios X e de radiação síncrotron.</p><p> É realizada também a prestação de serviços à comunidade na área de caracterização de materiais.</p>",                            "equipLab": [
                                {
                                    "nomeEquip": "Analisador de Porosidade",
                                    "imagemEquip": "Analisador_de_Porosidade.jpg"
                                },
                                {
                                    "nomeEquip": "Difratômetro de Monocristal",
                                    "imagemEquip": "Difratômetro_de_Monocristal.jpg"
                                },
                                {
                                    "nomeEquip": "Nanostar",
                                    "imagemEquip": "Nanostar.jpg"
                                }
                            ]
                        },
                        {
                            "nomeLab": "Análise de Materiais por Feixes Iônicos",
                            "imagemLab": "fap-lamfi.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos está apta a colaborar com o desenvolvimento e aplicação de métodos analíticos nucleares para análise e caracterização de materiais e superfícies. São realizados estudos de efeitos da topografia de filmes finos e porosidade de materiais em espectros RBS.</p><p> A equipe trabalha com precisão, acurácia e dependência da base de dados (secção de choque de ionização, poder de freamento e coeficientes de absorção de massa) de análises PIXE de pós, preparados na forma de amostras espessas. Além disso também são estudados fenômenos de passivação de superfícies contra oxidação, manutenção e aperfeiçoamento das instalações do LAMFI para disponibilizar e divulgar modernos recursos analíticos à comunidade científica.</p>",                            "equipLab": [
                                {
                                    "nomeEquip": "Acelerador e Fonte de Íons",
                                    "imagemEquip": "acelerador_e_fontes_de_ions.JPG"
                                }
                            ]

                        }
                    ]
                }

            ]
        },
        {
            "nome": "USP Leste",
            "nome_ext": "Universidade de São Paulo - Leste",
            "imagem": "./imagens/uspeach.jpg",
            "categorias": [{
                    "nomeCat": "Biologia",
                    "laboratorios": [{
                        "nomeLab": "Genômica e Evolução de Artrópodes",
                        "imagemLab": "genomica.jpg",
                        "descLab": "<p>Dentro do laboratório há acesso a pesquisadores e técnicos de diversas especialidades relacionadas à biologia, evolução de estruturas e genética. No laboratório de Genômica e evolução de Artrópodes podem ser realizados experimentos acerca da análise de manipulação genômica, tais como de células-tronco e doenças genéticas.</p><p> Quanto aos experimentos de evolução de artrópodes, há foco na descoberta de padrões de variação de genes e genomas para entender processos evolutivos subjacentes à emergência destes padrões. Para isso, usamos como organismos modelos os insetos e outros artrópodes.</p>",
                        "equipLab": [
                            {
                                "nomeEquip": "Cromatógrafo a gás",
                                "imagemEquip": "cromatografo-a-gas.jpg"
                            },
                            {
                                "nomeEquip": "Sequenciador",
                                "imagemEquip": "sequenciador.jpg"
                            }
                        ]
                    },
                        {
                            "nomeLab": "Antropologia Biológica",
                            "imagemLab": "antropologiabiologica.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos especializados está comprometida em compreender as interações entre genética, saúde e cultura de sociedades pretéritas sul-americanas. Atuando na área de bioarqueologia em seu aspecto transdisciplinar, temos como principal objeto de estudo o esqueleto humano.</p><p> Nossas linhas de pesquisa principais são a patologia óssea e a paledieta, realizando inspecção de alterações na forma dos elementos e impacto de processos biológicos, e também realizando a reconstrução de estruturas.</p><p> Esses processos podem ser aplicados a todo tipo de estrutura biológica ou com elementos biológicos.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Tomógrafo",
                                    "imagemEquip": "tomógrafo.jpg"
                                }
                            ]
                        }
                    ]
                },
                {
                    "nomeCat": "Química",
                    "laboratorios": [ {
                        "nomeLab": "Cristalografia",
                            "imagemLab": "fap-cristalografia.jpg",
                            "descLab": "<p>O Laboratório de Cristalografia tem como objetivos a pesquisa em física da matéria condensada, ciência dos materiais e áreas interdisciplinares com ênfase em propriedades estruturais: estudos de monocristais, policristais, sólidos amorfos, cristais líquidos, polímeros, géis, sistemas micelares e proteínas em solução.</p><p> São realizadas técnicas experimentais principalmente nas áreas de difração e espalhamento de raios X, associadas com outros métodos auxiliares.</p><p> É dada ênfase em materiais usados em dispositivos ópticos e eletrônicos e em sistemas complexos com ordem supra-molecular e utilizadas também fontes convencionais de raios X e de radiação síncrotron.</p><p> É realizada também a prestação de serviços à comunidade na área de caracterização de materiais.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Analisador de Porosidade",
                                    "imagemEquip": "Analisador_de_Porosidade.jpg"
                                },
                                {
                                    "nomeEquip": "Difratômetro de Monocristal",
                                    "imagemEquip": "Difratômetro_de_Monocristal.jpg"
                                },
                                {
                                    "nomeEquip": "Nanostar",
                                    "imagemEquip": "Nanostar.jpg"
                                }
                            ]
                        },
                        {
                            "nomeLab": "Análise de Materiais por Feixes Iônicos",
                            "imagemLab": "fap-lamfi.jpg",
                            "descLab": "<p>Nossa equipe de pesquisadores e técnicos está apta a colaborar com o desenvolvimento e aplicação de métodos analíticos nucleares para análise e caracterização de materiais e superfícies. São realizados estudos de efeitos da topografia de filmes finos e porosidade de materiais em espectros RBS.</p><p> A equipe trabalha com precisão, acurácia e dependência da base de dados (secção de choque de ionização, poder de freamento e coeficientes de absorção de massa) de análises PIXE de pós, preparados na forma de amostras espessas. Além disso também são estudados fenômenos de passivação de superfícies contra oxidação, manutenção e aperfeiçoamento das instalações do LAMFI para disponibilizar e divulgar modernos recursos analíticos à comunidade científica.</p>",
                            "equipLab": [
                                {
                                    "nomeEquip": "Acelerador e Fonte de Íons",
                                    "imagemEquip": "acelerador_e_fontes_de_ions.JPG"
                                }
                            ]

                        }
                    ]
                }
            ]
        }
    ]
}
