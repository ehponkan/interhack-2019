$(function() {
    var modal = $('.modal');
    var modalHeader = modal.find('.modal-header');
    var modalBody = modal.find('.modal-body');
    
    $(document).on('click', '.add-row', function() {
      var el = $(this).closest('.table-wrap').find('.row-name');
      appendRow(el);
    });
    $(document).on('keydown', '.row-name', function(e) {
      var el = $(this);
      if (e.keyCode == 13) {
        appendRow(el);
      }
    });
    function appendRow(el) {
      var table = el.closest('.table-wrap').find('table');
      var input = el.closest('.table-wrap').find('.table-inputs');
      if ( input.val() != '' ) {
        table.find('tbody').append('<tr class="'+input.val()+'"><td class="col-8">'+input.val()+'</td><td class="col-4"><label class="delete-row"><i class="fas fa-trash"></i> Delete</label></td></tr>');
        $('.row-name').find('input').val('');
      }
    }
    var row = null;
    $(document).on('click', '.delete-row', function() {
      row = $(this).closest('tr');
      modal.show();
      modalHeader.html('Delete ' + row.find('td:first').html() + '?');
      modalBody.html('Are you sure you want to delete row "' + row.find('td:first').html() + '"?');
      $(document).on('click', '.modal-save', function() {
        row.remove();
        modal.hide();
      });
      $(document).on('click', '.modal-cancel', function() {
        modal.hide();
      });
    });
    $(document).on('keyup', '#table-search', function() {
      var searchText = $(this).val().toLowerCase();
      var rows = $('table').find('tbody').find('tr');
      rows.each(function() {
        var row = this;
        var td = $(row).find('td:first');
        console.log(td.html());
        console.log(searchText);
        if( td.html().toLowerCase().indexOf(searchText) == -1 ) {
          $(row).hide();
        } else {
          $(row).show();
        }
      });
    });
    $(document).on('click', '.input-clear', function() {
      $(this).siblings('#table-search').val('');
      var rows = $('table').find('tbody').find('tr');
      rows.show();
    });
  });
  
  