let cat_inst = {
    "categorias":[
        {
            "nome": "Biologia",
            "instituicoes":[
                {
                    "nome":"USP Leste",
                    "nome_ext" : "Universidade de São Paulo - Leste",
                    "imagem":"uspeach.jpg"
                }
            ]
        },
        {
            "nome": "Física",
            "instituicoes":[
                {
                    "nome":"USP ICMC",
                    "nome_ext" : "Universidade de São Paulo - ICMC",
                    "imagem":"uspicmc.jpg"
                }
            ]
        },
        {
            "nome": "Ciências Farmacêuticas",
            "instituicoes":[
                {
                    "nome":"USP Butantã",
                    "nome_ext" : "Universidade de São Paulo - Butantã",
                    "imagem":"usp.jpg"
                }
            ]
        },
        {
            "nome": "Química",
            "instituicoes":[
                {
                    "nome":"USP Leste",
                    "nome_ext" : "Universidade de São Paulo - Leste",
                    "imagem":"uspeach.jpg"
                },
                {
                    "nome":"USP ICMC",
                    "nome_ext" : "Universidade de São Paulo - ICMC",
                    "imagem":"uspicmc.jpg"
                },
                {
                    "nome":"USP Butantã",
                    "nome_ext" : "Universidade de São Paulo - Butantã",
                    "imagem":"usp.jpg"
                }
            ]
        }
    ]
}
